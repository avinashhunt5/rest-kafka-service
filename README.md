# rest-kafka-service
## ---------------------------------------------------------

This service is responsible to take eligibility request send to kafka topic (eligibility-check-topic) which will be 
listened this service and store the request in H2 DB

### STARTING THE KAFKA and ZooKeeper service
## ----------------------------------------------------------

1. go to folder where the zookeeper directory and execute example
   [cd C:\Software\apache-zookeeper-3.7.0-bin\apache-zookeeper-3.7.0-bin\bin]
   [execute zkServer.cmd]

2. go to folder where the kafka directory 
   [cd C:\Software\kafka_2.12-2.8.0]
   [execute .\bin\windows\kafka-server-start.bat .\config\server.properties]
   
More details please refer https://dzone.com/articles/running-apache-kafka-on-windows-os

### H2 DB Details
## -----------------------------------------------------------

Once the kafka receiver receive message from topic (eligibility-check-topic) it will be stored in H2 DB, 
and the details can be seen at H2 DB console [http://localhost:9090/eligibility-kafka-service/h2-console]

### Building the service using -----> gradle clean build

### Running the service use   ------> java -jar eligibility-kafka-service-0.0.1-SNAPSHOT.jar

### Testing the service use
## ------------------------------------------------------------

http://localhost:9090/eligibility-kafka-service/kafka/validateEligibility

<?xml version="1.0" encoding="UTF-8"?>
<EligibilityRequest>
   <salary>1000</salary>
   <goldAsset>25000</goldAsset>
</EligibilityRequest>



