FROM alpine/git as clone
WORKDIR /app
RUN git clone https://avinashhunt5:March1234@gitlab.com/avinashhunt5/eligibility-kafka-service.git
 
FROM gradle:4.7.0-jdk8-alpine AS build
COPY --from=clone /app/rest-kafka-service /app
COPY --chown=gradle:gradle . /home/gradle/src
RUN cp -r /app/* /home/gradle/src/
WORKDIR /home/gradle/src

RUN gradle build --no-daemon
  
FROM openjdk:8-jre-slim
  
EXPOSE 8080
  
RUN mkdir /app
  
COPY --from=build /home/gradle/src/build/libs/*.jar /app/rest-kafka-service-0.0.1-SNAPSHOT.jar
WORKDIR /app
CMD java -jar rest-kafka-service-0.0.1-SNAPSHOT.jar
